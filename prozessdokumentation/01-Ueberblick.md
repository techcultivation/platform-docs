# Center for the Cultivation of Technology gGmbH
# Verfahrensdokumentation von Geschäftsvorfällen

Fassung vom 16.11.2016

## Überblick

Dieses Dokument dient der Dokumentation der elektronischen Systeme zur Erfassung von Geschäftsvorfällen in der Center for the Cultivation of Technology gGmbH. 

Geschäftsvorfälle werden von Mitarbeitern elektronisch angelegt und erhalten bei Erstellung eine fortlaufende Nummer. Technisch wird sichergestellt, dass Geschäftsvorfälle nur erweitert werden können, z.B. mit zusätzlichen Informationen angereichert oder als ungültig/gelöscht markiert werden können. Jede Änderung wird dabei systemseitig protokolliert und festgehalten, welcher Mitarbeiter die entsprechende Änderung eingebracht hat. (Unveränderbarkeit nach §146 Absatz 4 AO, §239 Absatz 3 HGB).

Vor der Ausführung des Geschäftsvorfalls erfolgt durch berechtigte Mitarbeiter eine Prüfung auf Vollständigkeit und Richtigkeit. Im folgenden werden diese speziell geschulten und autorisierten Personen als "Buchhalter" bezeichnet. Die Berechtigung zur Buchhaltung wird durch die Geschäftsführung erteilt und kann auf einzelne Geschäftsbereiche beschränkt sein (granulare Berechtigungen).

Zu einem vollständigen Geschäftsvorfall gehören ein oder mehrere Belege bzw. Sammelbelege. Belege und sonstige den Geschäftsvorfälle begleitenden Dokumente können in digitaler Form an einen Geschäftsvorfall angehängt werden. Technisch wird dabei sichergestellt, dass einmal eingebrachte Belege nicht mehr verändert werden können. Eine Nummerierung der hinzugefügten Dokumente, Belege und die Zuordnung zum Geschäftsfall erfolgt automatisch und fortlaufend nach der Freigabe durch die Buchhaltung.

Die Zuordnung zwischen einzelnen Belegen und der zugehörigen Buchung erfolgt durch Übernahme der bei Erfassung generierten Dokumentennummer in ein separates DV-System zur Buchhaltung. 

Das System gewährleistet die Möglichkeit zur Nachprüfung durch sachverständige Dritte im Rahmen steuerlicher Außenprüfungen.

## Elektronische Erfassung von Papierbelegen

Dokumente können grundsätzlich von jedem Mitarbeiter digitalisiert werden.

Nach dem Digitalisieren von Papierbelegen (z.B. durch Scan oder Photo) bewahrt der Einreichende den Beleg im Original bis zur Abschluss des Geschäftsvorgangs auf. Einreichende bestätigen bei jeder Einreichung, dass die Belege nicht anderweitig verwendet und nach Abschluss des Geschäftsvorgangs vernichtet werden. Sollte eine Erstattung von dritter Seite erfolgen, ist dies umgehend zu berichten.

Eine Qualitätskontrolle erfolgt vor Freigabe und Abschluss des Geschäftsvorgangs durch die Buchhaltung. Bei Unvollständigkeit oder mangelnder Qualität erfolgt eine entsprechende Benachrichtigung an den Einreicher. Dieser hat die Möglichkeit, z.B. verbesserte Scans dem Geschäftsvorfall hinzuzufügen, die dann wiederum neu geprüft werden.

## Datensicherheit

Eine Verbindung zwischen Mitarbeitern und DV-System erfolgt grundsätzlich über einen nach aktuellem Stand der Technik verschlüsselten Kanal. Mitarbeiter erhalten eine Zugangskennung, und werden entsprechend vom System identifiziert und Berechtigungen erteilt. Systemseitig wird sichergestellt, dass die Schnittstellen keine Möglichkeit der Manipulation zulassen. Ein Direktzugriff auf die Datenbank bzw. das Dateisystem wird softwareseitig verhindert.

Die erfassten Daten werden zeitnah so gesichert, dass eine durchgehende und vollständige Erfassung gewährleistet ist. Sicherungen werden verschlüsselt und manipulationssicher abgelegt. Die ordnungsgemässe Sicherung und die Möglichkeit der Wiederherstellung werden regelmässig geprüft und in einem Prüfprotokoll verzeichnet. 

